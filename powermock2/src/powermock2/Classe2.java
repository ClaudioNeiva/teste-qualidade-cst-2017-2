package powermock2;

import java.util.Scanner;

public class Classe2 {

	private static Scanner scanner = new Scanner(System.in);

	public int somar(int a, int b) {
		return Service.somar(a, b);
	}

	public void darBomDia() {
		System.out.println("Informe seu nome:");
		String nome = scanner.nextLine();
		System.out.println("Bom dia, " + nome + "!");
	}

}
