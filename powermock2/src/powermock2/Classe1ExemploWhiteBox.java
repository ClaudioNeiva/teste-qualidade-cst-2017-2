package powermock2;

public class Classe1ExemploWhiteBox {

	private int valor1;
	
	private int valor2;

	private int somar(int a, int b) {
		return a + b;
	}

	private int somarSemParametros() {
		return valor1 + valor2;
	}

}
