package powermock2;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Service.class, Classe2.class })
public class Classe2UnitarioTest {

	public Classe2 instancia2;

	@Before
	public void setup() {
		instancia2 = new Classe2();
	}

	@Test
	public void testarSomar() throws Exception {
		int a = 5;
		int b = 6;
		int somaEsperada = 11;

		PowerMockito.mockStatic(Service.class);

		PowerMockito.when(Service.somar(a, b)).thenReturn(11);

		int somaAtual = instancia2.somar(a, b);
		Assert.assertEquals(somaEsperada, somaAtual);
	}

	@Test
	public void testarDarBomDia() {

		// Criar um scanner mock
		Scanner scannerMock = PowerMockito.mock(Scanner.class);

		// Definir que o atributo scanner da Classe2 n�o ser� mais o instanciado
		// na declara��o do atributo, mas sim o mocado.
		Whitebox.setInternalState(Classe2.class, "scanner", scannerMock);

		// Criando um PrintStream mocado.
		PrintStream printStreamMock = Mockito.mock(PrintStream.class);
		// Definindo a sa�da padr�o do sistema para o PrintStream mocado.
		System.setOut(printStreamMock);

		// Ensinar que quando for chamado o nextline do scanner, � pra retornar
		// Claudio.
		PowerMockito.doReturn("Claudio").when(scannerMock).nextLine();

		// Chamar o m�todo que queremos testar
		instancia2.darBomDia();

		// Verificar se foi chamado o println, com a mensagem adequada.
		Mockito.verify(printStreamMock).println("Bom dia, Claudio!");
	}

}
