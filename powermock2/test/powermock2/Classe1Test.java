package powermock2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class Classe1Test {

	public Classe1ExemploWhiteBox instancia1;

	@Before
	public void setup() {
		instancia1 = new Classe1ExemploWhiteBox();
	}

	@Test
	public void testarSomar() throws Exception {
		int a = 5;
		int b = 6;
		int somaEsperada = 11;
		int somaAtual = Whitebox.invokeMethod(instancia1, "somar", a, b);
		Assert.assertEquals(somaEsperada, somaAtual);
	}

	@Test
	public void testarSomaSemParametros() throws Exception {
		int a = 15;
		int b = 26;
		int somaEsperada = 41;
		Whitebox.setInternalState(instancia1, "valor1", a);
		Whitebox.setInternalState(instancia1, "valor2", b);
		int somaAtual = Whitebox.invokeMethod(instancia1, "somar", a, b);
		Assert.assertEquals(somaEsperada, somaAtual);
	}

}
