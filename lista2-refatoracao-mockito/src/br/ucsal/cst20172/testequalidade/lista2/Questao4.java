package br.ucsal.cst20172.testequalidade.lista2;

public class Questao4 {

	/**
	 * Fun��o para c�lculo do fatorial.
	 * @param n - valor sobre o qual ser� calculado o fatorial
	 * @return valor do fatorial de n
	 */
	public static long calcularFatorial(int n) {
		long fat = 1;
		for (int i = 1; i <= n; i++) {
			fat *= i;
		}
		return fat;
	}

}
