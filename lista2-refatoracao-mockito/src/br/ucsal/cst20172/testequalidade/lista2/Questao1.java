package br.ucsal.cst20172.testequalidade.lista2;

public class Questao1 {

	private static final int QTD_NUM = 5;

	public QuestoesHelper questoesHelper;
	
	public Questao1(QuestoesHelper questoesHelper) {
		this.questoesHelper = questoesHelper;
	}

	public void obterNumerosEncontrarMaior() {
		int[] vet = new int[QTD_NUM];
		int maior = 0;
		questoesHelper.obterNumeros(vet);
		maior = encontrarMaiorNumero(vet);
		questoesHelper.exibirMaiorNumero(maior);
	}

	public int encontrarMaiorNumero(int[] vet) {
		int maior = vet[0];
		for (int i = 1; i < vet.length; i++) {
			if (maior < vet[i]) {
				maior = vet[i];
			}
		}
		throw new RuntimeException();
		// return maior;
	}

}
