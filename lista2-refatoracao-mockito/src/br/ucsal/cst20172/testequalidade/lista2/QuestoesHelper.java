package br.ucsal.cst20172.testequalidade.lista2;

import java.util.Scanner;

public class QuestoesHelper {

	public void obterNumeros(int[] vet) {
		Scanner tc = new Scanner(System.in);
		System.out.println("Digite os n�meros:");
		for (int i = 0; i < vet.length; i++) {
			vet[i] = tc.nextInt();
		}
	}

	public void exibirMaiorNumero(int maior) {
		System.out.println("Dentre os n�meros informados, o maior foi " + maior);
	}

	void inverterOrdemNumeros(int[] vet, int[] vetInvertida) {
		for (int i = 0; i < vet.length; i++) {
			vetInvertida[i] = vet[vet.length - i - 1];
		}

	}

	void exibirVetores(int[] vet, int[] vetInvertida) {
		System.out.println("Ordem normal: ");
		for (int i = 0; i < vet.length; i++) {
			System.out.print(vet[i] + " ");
		}

		System.out.println("\nOrdem invertida: ");
		for (int i = 0; i < vetInvertida.length; i++) {
			System.out.print(vetInvertida[i] + " ");
		}
	}
}
