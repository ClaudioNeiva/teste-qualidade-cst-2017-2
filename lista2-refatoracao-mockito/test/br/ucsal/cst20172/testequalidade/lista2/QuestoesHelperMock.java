package br.ucsal.cst20172.testequalidade.lista2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuestoesHelperMock extends QuestoesHelper {

	private List<String> metodosChamados = new ArrayList<>();

	@Override
	public void obterNumeros(int[] vet) {
		metodosChamados.add("obterNumeros");
		vet[0] = 45;
		vet[1] = 78;
		vet[2] = 97;
		vet[3] = 46;
		vet[4] = 12;
	}

	public int encontrarMaiorNumero(int[] vet) {
		if (Arrays.equals(vet, new int[] { 45, 78, 97, 46, 12 })) {
			metodosChamados.add("encontrarMaiorNumero");
			return 97;
		}
		return 0;
	}

	@Override
	public void exibirMaiorNumero(int maior) {
		if (maior == 97) {
			metodosChamados.add("exibirMaiorNumero");
		}
	}

	public void verificarChamadaMetodo(String nomeMetodo) throws MetodoNaoChamadoException {
		if (!metodosChamados.contains(nomeMetodo)) {
			throw new MetodoNaoChamadoException(nomeMetodo);
		}
	}

}
