package br.ucsal.cst20172.testequalidade.lista2;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoRule;

public class Questao1UnitarioComMOCKITOTest2 {

	private Questao1 questao1;

	@Mock
	private QuestoesHelper questoesHelperMock;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		questao1 = Mockito.spy(new Questao1(questoesHelperMock));
	}

	@Test
	public void obterNumerosEncontrarMaiorTeste1() throws MetodoNaoChamadoException {

		Mockito.doReturn(50).when(questao1).encontrarMaiorNumero(Mockito.any(int[].class));

		questao1.obterNumerosEncontrarMaior();

		Mockito.verify(questoesHelperMock).obterNumeros(Mockito.any(int[].class));

		Mockito.verify(questao1).encontrarMaiorNumero(Mockito.any(int[].class));

		Mockito.verify(questoesHelperMock).exibirMaiorNumero(50);
	}

}
