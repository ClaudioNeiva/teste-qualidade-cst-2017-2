package br.ucsal.cst20172.testequalidade.lista2;

import org.junit.Assert;
import org.junit.Test;

public class Questao4Test {

	@Test
	public void calcularFatorial5() {
		// Definir os dados de entrada (caixa preta)
		int n = 5;

		// Definir a sa�da esperada
		long fatorialEsperado = 120;

		// Executar o m�todo e obter o resultado atual
		long fatorialAtual = Questao4.calcularFatorial(n);

		// Comprar o resultado esperado com o resultado atual
		Assert.assertEquals(fatorialEsperado, fatorialAtual);
	}

}
