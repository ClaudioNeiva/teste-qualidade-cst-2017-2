package br.ucsal.cst20172.testequalidade.lista2;

import org.junit.Before;
import org.junit.Test;

public class Questao1UnitarioComStubTest {

	private Questao1 questao1;

	@Before
	public void setup() {
		QuestoesHelper questoesHelperStub = new QuestoesHelperStub();
		questao1 = new Questao1(questoesHelperStub);
	}

	@Test
	public void obterNumerosEncontrarMaiorTeste1() {

		questao1.obterNumerosEncontrarMaior();

		// Perguntar coisas do tipo:
		// Foi chamado o m�todo obterN�meros?
		// Foi chamado o m�todo encontrarMaiorNumero com o par�metro 45, 78, 97,
		// 46, 12?
		// Foi chamado o m�todo exibirMaiorNumero com o par�metro 97?
		// Como essa classe de testes utiliza stub, ela n�o vai conseguir
		// responder a essas perguntas.
	}

}
