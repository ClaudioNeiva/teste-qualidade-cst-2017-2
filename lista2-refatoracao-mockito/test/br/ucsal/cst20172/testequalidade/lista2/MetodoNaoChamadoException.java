package br.ucsal.cst20172.testequalidade.lista2;

public class MetodoNaoChamadoException extends Exception {

	private static final long serialVersionUID = 1L;

	public MetodoNaoChamadoException(String nomeMetodo) {
		super("M�todo: " + nomeMetodo);
	}

}
