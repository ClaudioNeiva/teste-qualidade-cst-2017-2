package br.ucsal.cst20172.testequalidade.lista2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class Questao1IntegradoTest {

	private static final String QUEBRA_LINHA = System.getProperty("line.separator");

	public Questao1 questao1;

	@Before
	public void setup() {
		QuestoesHelper questoesHelper = new QuestoesHelper();
		questao1 = new Questao1(questoesHelper);
	}

	@Test
	public void obterNumerosEncontrarMaiorTeste1() {

		// Dados de entrada
		String numerosInformados = "45\n78\n97\n46\n12";

		// Sa�da esperada
		String saidaEsperada = "Digite os n�meros:" + QUEBRA_LINHA + "Dentre os n�meros informados, o maior foi 97"
				+ QUEBRA_LINHA;

		// Subtituir o fluxo de entrada (System.in), cujo padr�o � teclado, por
		// um fluxo de entrada personalizado, com os dados que desejo para o
		// teste.
		InputStream inFake = new ByteArrayInputStream(numerosInformados.getBytes());
		System.setIn(inFake);

		// Subtituir o fluxo de sa�da (System.out), cujo padr�o � o console, por
		// um fluxo de sa�da personalizado, que permite uma verifica��o ap�s a
		// escrita do System.out.println.
		OutputStream outFake = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outFake));

		// Executa��o do m�todo que est� sendo testado e obten��o do resultado
		// atual
		questao1.obterNumerosEncontrarMaior();
		String saidaAtual = outFake.toString();

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(saidaEsperada, saidaAtual);
	}

}
