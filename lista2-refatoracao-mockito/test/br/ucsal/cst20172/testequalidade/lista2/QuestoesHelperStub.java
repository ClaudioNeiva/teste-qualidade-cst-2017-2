package br.ucsal.cst20172.testequalidade.lista2;

import java.util.Arrays;

public class QuestoesHelperStub extends QuestoesHelper {

	@Override
	public void obterNumeros(int[] vet) {
		vet[0] = 45;
		vet[1] = 78;
		vet[2] = 97;
		vet[3] = 46;
		vet[4] = 12;
	}

	public int encontrarMaiorNumero(int[] vet) {
		if (Arrays.equals(vet, new int[] { 45, 78, 97, 46, 12 })) {
			return 97;
		}
		return 0;
	}

	@Override
	public void exibirMaiorNumero(int maior) {
	}

}
