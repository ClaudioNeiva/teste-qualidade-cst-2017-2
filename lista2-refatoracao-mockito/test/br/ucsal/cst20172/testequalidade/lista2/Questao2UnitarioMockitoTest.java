package br.ucsal.cst20172.testequalidade.lista2;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class Questao2UnitarioMockitoTest {

	public Questao2 questao2;

	private QuestoesHelper questoesHelper;

	@Before
	public void setup() {
		questoesHelper = Mockito.mock(QuestoesHelper.class);
		questao2 = new Questao2(questoesHelper);
	}

	@Test
	public void obterNumerosInverterOrdem1Test() {

		questao2.obterNumerosInverterOrdem();

		// Verificar as chamadas
		Mockito.verify(questoesHelper).obterNumeros(Mockito.any(int[].class));
		Mockito.verify(questoesHelper).inverterOrdemNumeros(Mockito.any(int[].class), Mockito.any(int[].class));
		Mockito.verify(questoesHelper).exibirVetores(Mockito.any(int[].class), Mockito.any(int[].class));

	}
}
