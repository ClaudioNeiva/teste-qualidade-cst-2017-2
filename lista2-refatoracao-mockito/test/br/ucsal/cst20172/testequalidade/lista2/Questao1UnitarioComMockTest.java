package br.ucsal.cst20172.testequalidade.lista2;

import org.junit.Before;
import org.junit.Test;

public class Questao1UnitarioComMockTest {

	private Questao1 questao1;

	private QuestoesHelperMock questoesHelperMock;

	@Before
	public void setup() {
		questoesHelperMock = new QuestoesHelperMock();
		questao1 = new Questao1(questoesHelperMock);
	}

	@Test
	public void obterNumerosEncontrarMaiorTeste1() throws MetodoNaoChamadoException {

		questao1.obterNumerosEncontrarMaior();

		// Perguntar coisas do tipo:
		// Foi chamado o m�todo obterN�meros?
		questoesHelperMock.verificarChamadaMetodo("obterNumeros");
		// Foi chamado o m�todo encontrarMaiorNumero com o par�metro 45, 78, 97,
		// 46, 12?
		questoesHelperMock.verificarChamadaMetodo("encontrarMaiorNumero");
		// Foi chamado o m�todo exibirMaiorNumero com o par�metro 97?
		questoesHelperMock.verificarChamadaMetodo("exibirMaiorNumero");
	}

}
