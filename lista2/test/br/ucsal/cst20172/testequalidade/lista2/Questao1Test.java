package br.ucsal.cst20172.testequalidade.lista2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Test;

public class Questao1Test {

	private static final String QUEBRA_LINHA = System.getProperty("line.separator");

	/*
	 * M�todo que testa para um conjunto de entrada composto apenas de n�meros
	 * positivos, sendo que o maior est� na segunda posi��o.
	 */
	@Test
	public void encontrarMaiorNumeroPositivosSegundoMaiorTest() {
		// Dados de entrada
		int[] vet = { 4, 8, 7, 4, 3 };

		// Resultado esperado
		int maiorEsperado = 8;

		// Executa��o do m�todo que est� sendo testado e obten��o do resultado
		// atual
		Questao1 questao1 = new Questao1();
		int maiorAtual = questao1.encontrarMaiorNumero(vet);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(maiorEsperado, maiorAtual);
	}

	@Test
	public void verificarEntradaDados() {
		// Dados de entrada
		String numerosInformados = "45\n78\n97\n46\n12";

		// Resultado esperado
		int[] vetEsperado = { 45, 78, 97, 46, 12 };

		// Subtituir o fluxo de entrada (System.in), cujo padr�o � teclado, por
		// um fluxo de entrada personalizado, com os dados que desejo para o
		// teste.
		InputStream inFake = new ByteArrayInputStream(numerosInformados.getBytes());
		System.setIn(inFake);

		// Executa��o do m�todo que est� sendo testado e obten��o do resultado
		// atual
		int[] vetAtual = new int[5];
		Questao1 questao1 = new Questao1();
		questao1.obterNumeros(vetAtual);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertArrayEquals(vetEsperado, vetAtual);
	}

	@Test
	public void verificarMensagemNaEntradaDados() {
		// Dados de entrada
		String numerosInformados = "45\n78\n97\n46\n12";

		// Resultado esperado
		String mensagemEsperada = "Digite os n�meros:" + QUEBRA_LINHA;

		// Subtituir o fluxo de entrada (System.in), cujo padr�o � teclado, por
		// um fluxo de entrada personalizado, com os dados que desejo para o
		// teste.
		InputStream inFake = new ByteArrayInputStream(numerosInformados.getBytes());
		System.setIn(inFake);

		// Subtituir o fluxo de sa�da (System.out), cujo padr�o � o console, por
		// um fluxo de sa�da personalizado, que permite uma verifica��o ap�s a
		// escrita do System.out.println.
		OutputStream outFake = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outFake));

		// Executa��o do m�todo que est� sendo testado e obten��o do resultado
		// atual
		int[] vet = new int[5];
		Questao1 questao1 = new Questao1();
		questao1.obterNumeros(vet);
		String mensagemAtual = outFake.toString();

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(mensagemEsperada, mensagemAtual);
	}

}
