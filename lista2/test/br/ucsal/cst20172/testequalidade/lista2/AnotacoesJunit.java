package br.ucsal.cst20172.testequalidade.lista2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AnotacoesJunit {

	@BeforeClass
	public static void setupClasse() {
		System.out.println("\nexecutar UM �NICA VEZ ANTES do PRIMEIRO teste executar para a CLASSE atual...");
	}

	@AfterClass
	public static void teardownClasse() {
		System.out.println("\nexecutar UM �NICA VEZ DEPOIS do �LTIMO teste executar para a CLASSE atual...");
	}

	@Before
	public void setupMetodo() {
		System.out.println("\nexecutar um c�digo SEMPRE ANTES de executar cada teste...");
	}

	@After
	public void teardownMetodo() {
		System.out.println("executar um c�digo SEMPRE DEPOIS de executar cada teste...");
	}

	@Test
	public void teste1() {
		System.out.println("teste 1 ...");
	}

	@Test
	public void teste2() {
		System.out.println("teste 2 ...");
	}

	@Test
	public void teste3() {
		System.out.println("teste 3 ...");
	}

	@Test
	public void teste4() {
		System.out.println("teste 4 ...");
	}

}
