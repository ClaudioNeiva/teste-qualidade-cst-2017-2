package br.ucsal.cst20172.testequalidade.lista2;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

// https://github.com/junit-team/junit4/wiki/parameterized-tests

@RunWith(Parameterized.class)
public class ExemploTesteJunitParametrizado {
	
	@Parameters(name = "teste fibonacci {index}: fib({0})={1}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { 0, 0 }, { 1, 1 }, { 2, 1 }, { 3, 2 }, { 4, 3 }, { 5, 5 }, { 6, 8 } });
	}

	private int fInput;

	private int fExpected;

	public ExemploTesteJunitParametrizado(int input, int expected) {
		fInput = input;
		fExpected = expected;
	}

	@Test
	public void test() {
		assertEquals(fExpected, Fibonacci.compute(fInput));
	}
}
