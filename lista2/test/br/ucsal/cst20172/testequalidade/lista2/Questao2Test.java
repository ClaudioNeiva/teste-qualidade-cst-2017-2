package br.ucsal.cst20172.testequalidade.lista2;


import org.junit.Assert;
import org.junit.Test;

public class Questao2Test {

    // Comentário novo 1
	@Test
	public void inverterOrdemNumerosTest() {
		// Dados de entrada
		int[] vet = { 7, 9, 12, 5, 2 };

		// Resultado esperado
		int[] vetInvertidoEsperado = { 2, 5, 12, 9, 7 };

		// Executação do método que está sendo testado e obtenção do resultado
		// atual
		Questao2 questao2 = new Questao2();
		int[] vetInvertidoAtual = new int[5];
		questao2.inverterOrdemNumeros(vet, vetInvertidoAtual);

		// Comparação do resultado esperado com o resultado atual
		Assert.assertArrayEquals(vetInvertidoEsperado, vetInvertidoAtual);

	}
}
