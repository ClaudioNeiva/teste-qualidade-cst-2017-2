package br.ucsal.cst20172.testequalidade.lista2;

import java.util.Scanner;

public class Questao1 {

	private static final int QTD_NUM = 5;

	void obterNumerosEncontrarMaior() {
		int[] vet = new int[QTD_NUM];
		int maior;
		obterNumeros(vet);
		maior = encontrarMaiorNumero(vet);
		exibirMaiorNumero(maior);
	}

	void obterNumeros(int[] vet) {
		Scanner tc = new Scanner(System.in);
		System.out.println("Digite os n�meros:");
		for (int i = 0; i < vet.length; i++) {
			vet[i] = tc.nextInt();
		}
	}

	int encontrarMaiorNumero(int[] vet) {
		int maior = vet[0];
		for (int i = 1; i < vet.length; i++) {
			if (maior < vet[i]) {
				maior = vet[i];
			}
		}
		return maior;
	}

	void exibirMaiorNumero(int maior) {
		System.out.println("Dentre os n�meros informados, o maior foi " + maior);
	}

}
