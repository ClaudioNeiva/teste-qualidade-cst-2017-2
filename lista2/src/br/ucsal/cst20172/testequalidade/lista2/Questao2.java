package br.ucsal.cst20172.testequalidade.lista2;

import java.util.Scanner;

public class Questao2 {

	private static final int QTD_NUM = 5;

	private static Scanner tc = new Scanner(System.in);

	public static void main(String[] args) {
		Questao2 questao2 = new Questao2();
		questao2.obterNumerosInverterOrdem();
	}

	void obterNumerosInverterOrdem() {
		int[] vet = new int[QTD_NUM];
		int[] vetInvertida = new int[QTD_NUM];
		obterNumeros(vet);
		inverterOrdemNumeros(vet, vetInvertida);
		exibirVetores(vet, vetInvertida);
	}

	void obterNumeros(int[] vet) {

		System.out.println("Informe os 5 n�meros: ");
		for (int i = 0; i < vet.length; i++) {
			vet[i] = tc.nextInt();
		}
	}

	void inverterOrdemNumeros(int[] vet, int[] vetInvertida) {
		for (int i = 0; i < vet.length; i++) {
			vetInvertida[i] = vet[vet.length - i - 1];
		}

	}

	void exibirVetores(int[] vet, int[] vetInvertida) {
		System.out.println("Ordem normal: ");
		for (int i = 0; i < vet.length; i++) {
			System.out.print(vet[i] + " ");
		}

		System.out.println("\nOrdem invertida: ");
		for (int i = 0; i < vetInvertida.length; i++) {
			System.out.print(vetInvertida[i] + " ");
		}
	}

}
