package br.ucsal.cst20172.testequalidade.lista2;

public class Fibonacci {
	public static int compute(int n) {
		int result = 0;

		if (n <= 1) {
			result = n;
		} else {
			result = compute(n - 1) + compute(n - 2);
		}

		return result;
	}
}